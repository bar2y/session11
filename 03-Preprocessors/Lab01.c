/*
 ============================================================================
 Name        : Lab01.c
 Author      : Khaled Naga
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that takes 2 input numbers i.e. x and y; and
 do the following:
 + Check value of given x is less that a predefined value
 + Check value of given y is less that a predefined value
 + Print the result of x + y
 + Print the result of x * y
 + Print the result of (x + y) * x
 + Use Macros to hold the predefined values of x and y
 + Use Macro to hold addition formula
 + Use Macro to hold multiplication formula
 ============================================================================
*/

#include <stdio.h>

int main(void)
{
	
	return 0;
}
