#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    // type casting examples
    // example 1
    /*float x = 1.1;
    double r = 0;
    int y = 5;

    r = x + y;// the result of this is unexpected, dependent on the machine
    printf("%f", r);// try changing the specifier*/

    //********************************************************
    //example 2
    /*char x; //0b 0000 1010 or oX 0A
    int y = 150;
    x = y;
    printf("%d" , x);// most significant bits are lost*/

    //********************************************************************
    //example 3
    /*volatile int x = 1;
    scanf("%d",&x);
    if(x == 1){
        printf("Correct");
    }else{
        printf("Wrong");

    }*/

    unsigned char x;
    unsigned int result = 1;
    scanf("%d", &x);
    for(x; x >0; x--){
        result = result * x;
    }
    printf("the answer is %d \n", result);

}
